/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdw.pkg05.order.client;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public class Mdw05OrderClient {
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws Exception {
		GenericProducer prod = new GenericProducer();
		
		prod.send("jms/orders", "bookingname|ashfkj|fashjkasdfa");
		prod.send("jms/orders", "newtripname|dsfgf|fd");
		prod.send("jms/orders", "newtripname|w53|432");
	}
	
}
