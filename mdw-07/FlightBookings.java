package cz.zipek.mdw;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public class FlightBookings {
	private final Map<Integer, FlightBooking> bookings = new HashMap<>();
	private int idCounter = 0;
	
	public Collection<FlightBooking> getAll() {
		return bookings.values();
	}
	
	public FlightBooking get(int id) {
		return bookings.get(id);
	}
	
	public void set(FlightBooking booking) {
		bookings.put(booking.getId(), booking);
	}
	
	public void add(FlightBooking booking) {
		booking.setId(idCounter++);
		set(booking);
	}
	
	public void delete(FlightBooking booking) {
		bookings.remove(booking.getId());
	}
}
