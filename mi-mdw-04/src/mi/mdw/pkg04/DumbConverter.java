/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.mdw.pkg04;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public class DumbConverter extends UnicastRemoteObject implements CurrencyConverter {
	private final Map<String, Double> rates;
	
	public DumbConverter() throws RemoteException {
		super();
		
		rates = new HashMap<>();
		
		rates.put("EUR", 0.8);
		rates.put("CZK", 24.5);
		rates.put("RUB", 1E10);
	}
	
	@Override
	public double convert(String from, String to, double amount) throws RemoteException {
		double original = amount;
		
		from = from.toUpperCase();
		to = to.toUpperCase();
		
		if (!from.equals("USD")) {
			if (!rates.containsKey(from)) {
				original *= 1/rates.get(from);
			} else {
				return -1;
			}
		}
		
		if (!rates.containsKey(to)) {
			return -1;
		}
		
		return original * rates.get(to);
	}
}
