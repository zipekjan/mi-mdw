/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdw.pkg05.trips.processor;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public class Mdw05TripsProcessor {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws Exception {
		(new TripsProcessor()).receive("jms/newtrips");
	}
	
}
