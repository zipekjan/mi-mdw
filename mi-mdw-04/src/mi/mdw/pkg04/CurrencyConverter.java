/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.mdw.pkg04;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public interface CurrencyConverter extends Remote {
	public double convert(String from, String to, double amount) throws RemoteException;
}
