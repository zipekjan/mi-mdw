/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.mdw.pkg04.client;

import java.rmi.Naming;
import mi.mdw.pkg04.CurrencyConverter;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public class MiMdw04Client {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws Exception {
		CurrencyConverter converter = (CurrencyConverter)Naming.lookup("//0.0.0.0/converter");
		
		System.out.println(converter.convert("USD", "CZK", 125));
		System.out.println(converter.convert("USD", "EUR", 125));
		System.out.println(converter.convert("USD", "RUB", 125));
		System.out.println(converter.convert("RUB", "USD", 125));
	}
	
}
