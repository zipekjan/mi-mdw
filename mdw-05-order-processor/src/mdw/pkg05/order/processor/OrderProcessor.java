/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdw.pkg05.order.processor;

import java.util.logging.Level;
import java.util.logging.Logger;
import mdw.pkg05.order.client.GenericConsumer;
import mdw.pkg05.order.client.GenericProducer;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public class OrderProcessor extends GenericConsumer {
	@Override
	protected void accept(String text) {
		try {
			String ident = text.substring(0, 7).toLowerCase();
			String data = text.substring(7);
			GenericProducer prod = new GenericProducer();
			
			switch(ident) {
				case "booking":
					prod.send("jms/bookings", data);
					break;
				case "newtrip":
					prod.send("jms/newtrips", data);
					break;
			}
		} catch (Exception ex) {
			Logger.getLogger(OrderProcessor.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
