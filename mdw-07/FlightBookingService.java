package cz.zipek.mdw;

import java.util.Collection;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
@WebService
public class FlightBookingService {
	private final FlightBookings bookings = new FlightBookings();
	
	public FlightBooking createBooking(@WebParam(name="person") String person, @WebParam(name="departure") String departure, @WebParam(name="arrival") String arrival, @WebParam(name="airport") String airport) {
		FlightBooking book = new FlightBooking(person, departure, arrival, airport);
		bookings.add(book);
		return book;
	}
	
	public boolean deleteBooking(@WebParam(name="id") int id) {
		FlightBooking book = bookings.get(id);
		if (book == null) {
			return false;
		}
		
		bookings.delete(book);
		return true;
	}
	
	public FlightBooking updateBooking(@WebParam(name="booking") FlightBooking updated) {
		FlightBooking book = bookings.get(updated.getId());
		if (book == null) {
			return null;
		}
		
		book.setPassengerName(updated.getPassengerName());
		book.setDeparture(updated.getDeparture());
		book.setArrival(updated.getArrival());
		book.setAirport(updated.getAirport());
		
		bookings.set(book);
		
		return book;
	}

	public Collection<FlightBooking> getBookings() {
		return bookings.getAll();
	}
	
	public FlightBooking getBooking(@WebParam(name="id") int id) {
		return bookings.get(id);
	}

}
