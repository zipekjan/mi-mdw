/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdw.pkg05.order.client;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public class Config {
    // Defines the JNDI context factory.
    public final static String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
 
    // Defines the JMS context factory.
    public final static String JMS_FACTORY = "jms/mdw-cf";
 
    // URL
    public final static String PROVIDER_URL = "t3://localhost:7001";
}