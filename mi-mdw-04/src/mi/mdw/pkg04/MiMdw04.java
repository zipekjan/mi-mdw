/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mi.mdw.pkg04;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 *
 * @author Jan Zípek <jan at zipek.cz>
 */
public class MiMdw04 {
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		try {
            LocateRegistry.createRegistry(1099);
 
            DumbConverter server = new DumbConverter();
            Naming.rebind("//0.0.0.0/converter", server);
 
            System.out.println("Server started...");
        } catch (Exception e) {
            System.out.println("Error: " + e.getLocalizedMessage());
        }
	}
}
